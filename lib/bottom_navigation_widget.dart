import 'package:flutter/material.dart';
import 'pages/airplay_screen.dart';
import 'pages/email_screen.dart';
import 'pages/home_screen.dart';
import 'pages/page_screen.dart';

class BottomNavigationWidget extends StatefulWidget {
  @override
  _BottomNavigationWidgetState createState() => _BottomNavigationWidgetState();
}

class _BottomNavigationWidgetState extends State<BottomNavigationWidget> {

  final _BottomNavigationColor=Colors.blue;
  int _currentIndex=0; //show current tag on the bottom bar, start from 0->home, 1->email etc
  List<Widget> list=List(); // treat as array but type should be Widget

  //override state of 4 widgets
  @override
  void initState(){
    list
      ..add(HomeScreen())
      ..add(EmailScreen())
      ..add(AirplayScreen())
      ..add(PageScreen());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: list[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          // home
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: _BottomNavigationColor,
            ),
            title: Text(
              'Home',
              style:TextStyle(color: _BottomNavigationColor)
            )
          ),
          // email
          BottomNavigationBarItem(
            icon: Icon(
              Icons.email,
              color: _BottomNavigationColor,
            ),
            title: Text(
              'Email',
              style:TextStyle(color: _BottomNavigationColor)
            )
          ),
          // page
          BottomNavigationBarItem(
            icon: Icon(
              Icons.pages,
              color: _BottomNavigationColor,
            ),
            title: Text(
              'Pages',
              style:TextStyle(color: _BottomNavigationColor)
            )
          ),
          //airplay
          BottomNavigationBarItem(
            icon: Icon(
              Icons.airplay,
              color: _BottomNavigationColor,
            ),
            title: Text(
              'Airplay',
              style:TextStyle(color: _BottomNavigationColor)
            )
          )
        ],
        currentIndex: _currentIndex,
        onTap:(int index){
          setState(() {
           _currentIndex=index; 
          });
        },
      ),
    );
  }
}